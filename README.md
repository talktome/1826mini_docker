# Docker image with statically linked Golang binary

Based on [article](http://sebest.github.io/post/create-a-small-docker-image-for-a-golang-binary) by Sebastien Estienne.

A Docker image without Linux! Just a static binary and a couple of dependencies.

Usage:

	docker build -t demo https://bitbucket.org/talktome/1826mini_docker.git
	docker run --rm demo

	docker images demo  # for size info
