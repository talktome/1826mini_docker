FROM golang:1.10 AS builder
WORKDIR /
COPY main.go .
RUN env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags='-s -w' main.go
RUN curl -o ca-certificates.crt https://raw.githubusercontent.com/bagder/ca-bundle/master/ca-bundle.crt

FROM scratch
COPY --from=builder /main .
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /ca-certificates.crt /etc/ssl/certs/
CMD ["/main"]
